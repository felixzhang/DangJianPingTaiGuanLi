<?php

global $_GPC,$_W;
$id = $_GPC['id'];
require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";
if(!isUser()){
    $url =$this->createMobileUrl('login');
    header("location: $url");
    setJumpUrl();
    die();
}
$user= getUser();

$request = postCurl(getServer()."/getPartyactivitiesById",array(
    "id"=>$id,
    "memberid"=>$user['id']
));

$host = $request['host'];
//var_dump($host);
$data = $request['data'];
$item = $data['list'][0];
$status=$item['sign'];
include $this->template('activityDetail');