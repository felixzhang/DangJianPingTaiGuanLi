<?php
require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";

global $_W,$_GPC;
$weid=$_W['uniacid'];//获取当前公众号ID
$openid=$_W['openid'];//获得当前用户ID
$pid=$params['tid'];//获取订单ID

/*判断输入金额与应付金额是否相同*/
if($params['result'] == 'success' && $params['from'] == 'notify'){
    $request = postCurl(getServer()."/PayCallback",array(
        "payid"=>$pid
    ));
}

/*调微擎支付页面，并跟新未支付订单为已支付*/
if ($params['from'] == 'return') {
    $request = postCurl(getServer()."/PayCallback",array(
        "payid"=>$pid
    ));
    message('支付成功！', '../../app/' . $this->createMobileUrl('payParty'), 'success');
}
