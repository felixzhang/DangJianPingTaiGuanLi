<?php
/**
 * 党员管理系统模块微站定义
 *
 * @author qq544386107
 * @url 
 */
defined('IN_IA') or exit('Access Denied');

class Party_memberModuleSite extends WeModuleSite {

	public function doMobileLogin() {
		//这个操作被定义用来呈现 功能封面
        require_once (dirname(__FILE__)."/inc/doMobileLogin.php");
	}
    public function doMobilePwdModify() {
        //这个操作被定义用来呈现 功能封面
        require_once (dirname(__FILE__)."/inc/doMobilePwdModify.php");
    }
    public function doMobileEvaluation() {
        //手机端考评页
        require_once (dirname(__FILE__)."/inc/doMobileEvaluation.php");
    }

    public function doMobilePayParty() {
	    //党费缴纳
        require_once (dirname(__FILE__)."/inc/doMobilePayParty.php");
    }

    public function doMobileActivities() {
        //手机端党员活动页面
        require_once(dirname(__FILE__) . "/inc/doMobileActivities.php");
    }

    public function doMobileAjaxSign(){
	    //异步报名接口
        require_once (dirname(__FILE__)."/inc/doMobileAjaxSign.php");
    }

    public function doMobileNoticeReview() {
        //手机端通知公告页面
        require_once (dirname(__FILE__)."/inc/doMobileNoticeReview.php");

    }
    public function doMobileActivityDetail(){
        //手机端党员活动详情
        require_once (dirname(__FILE__)."/inc/doMobileActivityDetail.php");
    }

    public function doMobileVolunteerActivity(){
        //手机端志愿活动详情
        require_once (dirname(__FILE__)."/inc/doMobileVolunteerActivity.php");
    }


    public function doMobileNoticeDetail(){
        require_once (dirname(__FILE__)."/inc/doMobileNoticeDetail.php");
    }
    public function doMobileReviewDetail(){
        //手机端民主评议
        require_once (dirname(__FILE__)."/inc/doMobileReviewDetail.php");
    }

    public function doMobileJoinParty(){
        //手机端入党申请页
        require_once (dirname(__FILE__)."/inc/doMobileJoinParty.php");
    }

    public function doMobileJoinSuccess(){
        //手机端入党申请成功页面
        require_once (dirname(__FILE__)."/inc/doMobileJoinSuccess.php");
    }

    public function doMobileClassRoom(){
        //手机端移动课堂页
        require_once (dirname(__FILE__)."/inc/doMobileClassRoom.php");
    }

    public function doMobileClassDetail(){
        //手机端移动课堂详情页
        require_once (dirname(__FILE__)."/inc/doMobileClassDetail.php");
    }
    public function doMobileGoodPattern(){
        //手机端移动好榜样页面
        require_once (dirname(__FILE__)."/inc/doMobileGoodPattern.php");
    }

    public function doMobilePatternDetail(){
        //手机端移动好榜样详情页
        require_once (dirname(__FILE__)."/inc/doMobilePatternDetail.php");
    }
    public function doMobileBranchStyle(){
        //手机端移动支付风采页面
        require_once (dirname(__FILE__)."/inc/doMobileBranchStyle.php");
    }

    public function doMobilePartyStyle(){
        //手机端移动支付风采页面
        require_once (dirname(__FILE__)."/inc/doMobilePartyStyle.php");
    }

    public function doMobileEnterprise(){
        //手机端移动企业之窗页面
        require_once (dirname(__FILE__)."/inc/doMobileEnterprise.php");
    }
    public function doMobileEnterpriseInfo(){
        //手机端移动企业心虚港
        require_once (dirname(__FILE__)."/inc/doMobileEnterpriseInfo.php");
    }

    public function doMobileProduct(){
        //手机端移动产品信息
        require_once (dirname(__FILE__)."/inc/doMobileProduct.php");
    }


    public function doMobileRecruit(){
        //手机端移动招聘信息
        require_once (dirname(__FILE__)."/inc/doMobileRecruit.php");
    }

    public function doMobileDiscuss(){
        //手机端移动评论
        require_once (dirname(__FILE__)."/inc/doMobileDiscuss.php");
    }

    public function doMobileLoginAjax() {
        //手机端登录ajax
        require_once (dirname(__FILE__)."/inc/doMobileLoginAjax.php");
    }
    public function doMobileJoinAjax() {
        //手机端入党ajax
        require_once (dirname(__FILE__)."/inc/doMobileJoinAjax.php");
    }

    public function doMobileRepwdAjax(){
        //手机端修改密码ajax
        require_once (dirname(__FILE__)."/inc/doMobileRepwdAjax.php");
    }

    public function doMobileDiscussAjax(){
        //手机端评论ajax
        require_once (dirname(__FILE__)."/inc/doMobileDiscussAjax.php");
    }

    public function doMobileExcellentLoveAjax(){
        //优秀榜点赞
        require_once (dirname(__FILE__)."/inc/doMobileExcellentLoveAjax.php");
    }

    public function doMobileBranchLoveAjax(){
        //支部点赞
        require_once (dirname(__FILE__)."/inc/doMobileBranchLoveAjax.php");
    }


    public function doMobilePartyLoveAjax(){
        //党员点赞
        require_once (dirname(__FILE__)."/inc/doMobilePartyLoveAjax.php");
    }

    public function doMobilereviewAjax(){
        require_once (dirname(__FILE__)."/inc/doMobilereviewAjax.php");
    }



    public function doMobileGetCode(){
        //获取验证码
        require_once (dirname(__FILE__)."/inc/getCode.php");
    }

    public function doMobilePay(){
        /*手机端支付*/
        require_once (dirname(__FILE__)."/inc/doMobilePay.php");
    }

    /*web端付款放回结果*/
    public function payResult($params){
        require_once  (dirname(__FILE__)."/inc/doMobilePayResult.php" );
    }


	public function doWebLogin() {
		//这个操作被定义用来呈现 规则列表


	}
	public function doWebManager1() {
		//这个操作被定义用来呈现 管理中心导航菜单
	}
	public function doWebManager2() {
		//这个操作被定义用来呈现 管理中心导航菜单
	}
}