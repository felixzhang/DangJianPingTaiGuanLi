<?php

require_once dirname(__FILE__)."/api.php";


function getUser(){
    return $_SESSION['dx_user'];
}

function setUser($user){
    $_SESSION['dx_user'] = $user;
}

function isUser(){
    global $_W;
    if($_SESSION['dx_user'] == null)
    {
        //利用openid获取用户信息
        $openid= $_W['openid'];
        $request = postCurl(getServer()."/loginByOpenid",array(
            "openid"=>$openid
        ));
        if($request['ret']!=0){
            return false;
        }else{
            setUser($request['data']);
            return true;
        }
    }
    else
    {
        //return $_SESSION['dx_user']!=null;
        return ture;
    }
}

function outUser($user){
    $_SESSION['dx_user']=null;
}

function getUserInfo(){
    $user = getUser();
    $userInfo = postCurl(getServer()."/getPartymemberByid",array(
        "id"=>$user["id"],
    ));

    return $userInfo['data'];
}


function is_https() {
    if ( !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
        return true;
    } elseif ( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
        return true;
    } elseif ( !empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off') {
        return true;
    }
    return false;
}

function setJumpUrl(){
    $url = "";
    if(is_https()){
        $url.="https://";
    }else{
        $url.="http://";
    }
    $url.=$_SERVER['HTTP_HOST'];
    if($_SERVER["SERVER_PORT"]!=80){
        $url.=$url.":".$_SERVER["SERVER_PORT"];
    }
    $url.=$_SERVER["REQUEST_URI"];
    $_SESSION['jumpurl']=$url;
}
function getJumpUrl(){
    return $_SESSION['jumpurl'];
}